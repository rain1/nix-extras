with import <nixpkgs> {}; # bring all of Nixpkgs into scope

stdenv.mkDerivation rec {
  name = "enigma-${version}";
  version = "1.21";
  
  src = fetchurl {
    url = "mirror://sourceforge/enigma-game/Release%20${version}/${name}.tar.gz";
    sha256 = "00ffh9pypj1948pg3q9sjp1nmiabh52p5c8wpg9n1dcfgl3cywnq"; # 1.21
    #sha256 = "12vm34fr088c5r70slnlw43i268fnmi3fkw8xy2vl7vsldvj7bpj"; # 1.20
  };

  buildInputs = [ SDL SDL_image SDL_mixer SDL_ttf libpng xercesc curl imagemagick ];
  patches = [
    ./enigma-1.21-ifs-null.patch
  ];
}
