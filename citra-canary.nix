with import <nixpkgs> {};
# { stdenv, fetchgit, cmake, SDL2, qtbase, qtmultimedia, boost, curl, gtest }:

stdenv.mkDerivation rec { 
  name = "citra-${version}";
  version = "canary";

  # Submodules
  src = fetchgit {
	url = "https://github.com/citra-emu/citra-canary";
	rev = "11c5c74fdf2ad8506e710b94e291bd7dedd3efdb";
	branchName = "canary";
	sha256 = "1lbsazvjy69ml7vp1wmz6yn4d2dcly0kizldzgf0g47v2xj73w0f";
  };

  enableParallelBuilding = true;
  nativeBuildInputs = [ cmake ];
  buildInputs = [ makeWrapper SDL2 qt5.qtbase qt5.qtmultimedia boost curl gtest ];
  cmakeFlags = [ "-DUSE_SYSTEM_CURL=ON" "-DUSE_SYSTEM_GTEST=ON" ];

  preConfigure = ''
    # Trick configure system.
    sed -n 's,^ *path = \(.*\),\1,p' .gitmodules | while read path; do
      mkdir "$path/.git"
    done
  '';

  postFixup = ''
    wrapProgram $out/bin/citra-qt \
      --prefix QT_PLUGIN_PATH : "${qt5.qtbase}/${qt5.qtbase.qtPluginPrefix}"
  '';

  meta = with stdenv.lib; {
    homepage = "https://citra-emu.org";
    description = "An open-source emulator for the Nintendo 3DS";
    license = licenses.gpl2;
    maintainers = with maintainers; [ abbradar ];
    platforms = platforms.linux;
  };
}
